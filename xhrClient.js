const Axios = require('axios');

//TODO: npm run build is not picking up .env variables and as such the issues when running locally
//during deployment they would any how be available so, so not making changes to make them available using .env

const token = localStorage.getItem('x-auth-token') || '';

export const xhrClient = Axios.create({
    baseURL: process.env.API_SERVER ? process.env.API_SERVER : "http://localhost:4000/api/",
    headers: { 'Access-Control-Allow-Origin': '*', 'x-auth-token': token },
    withCredentials: true
});

export const xhrClientWithoutCreds = Axios.create({
    baseURL: process.env.API_SERVER ? process.env.API_SERVER : "http://localhost:4000/api/",
    headers: { 'Access-Control-Allow-Origin': '*' },
});