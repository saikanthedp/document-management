
import 'materialize-css/dist/css/materialize.min.css'

import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';

import DocTreeView from './components/DocTreeView';
import Login from './components/Login';

function App() {
  const isLoggedIn = localStorage.getItem('isLoggedIn') || false;
  const user = JSON.parse(localStorage.getItem('user') || '{}');
  const { Name } = user;

  const doLogout = () => {
    localStorage.clear();

    window.location.href = "/login";
  }

  return (
    <div className="App">      
      <Router>
        <Switch>
          <Route path="/login" exact>
            {isLoggedIn ? <Redirect to="/home" /> : <Login />}
          </Route>
          <Route path="/*">
            <header className="App-header">
              <div className="row">
                <div className="col s3 text-left">
                  <img src={logo} className="App-logo" alt="logo" />  
                </div>
                <div className="col s9 text-right">
                  Hi, {Name}! &nbsp;
                  <Button variant="contained" color="secondary" onClick={doLogout} style={{marginTop: 30, marginBottom: 30}}>
                    Logout
                  </Button>  
                </div>
              </div>      
            </header>
            <Container component="main" maxWidth="md">
              <Route path="/home" component={DocTreeView} />
            </Container>
            {isLoggedIn ? <Redirect to="/home" /> : <Redirect to="/login" />}
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
