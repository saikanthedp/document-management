import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import Loader from '../Loader';
import { xhrClientWithoutCreds } from '../../xhrClient';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const [uname, setUname] = React.useState('');
  const [pwd, setPwd] = React.useState('');

  const doLogin = ($event) => {
    $event.preventDefault();
    setOpen(true);

    xhrClientWithoutCreds
      .post('user/login', {uname, pwd})
      .then(res => {
        const data = res.data || {};

        if(data.success === true) {
          const userInfo = data.data || {};

          localStorage.setItem('x-auth-token', userInfo.token);
          localStorage.setItem('user', JSON.stringify(userInfo.user));
          localStorage.setItem('isLoggedIn', true);

          window.location.href = '/home';
        } else {
          alert('Invalid login details were provided.');
        }

        setOpen(false);
      })
      .catch(error => {
        alert('Something went wrong, Please try again after sometime...');
        setOpen(false);
      })
  }

  const setUsername = ($event) => {
    const value = $event.target.value;
    setUname(value);
  }

  const setPassword = ($event) => {
    const value = $event.target.value;
    setPwd(value);
  }

  return (
    <Container component="main" maxWidth="xs">      
      <div className={classes.paper}>
        <Loader open={open} />
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={($event) => doLogin($event)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={($event) => setUsername($event)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={($event) => setPassword($event)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
        </form>
      </div>
    </Container>
  );
}