
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import { TreeItem } from '@material-ui/lab';
import { MinusSquare, PlusSquare } from '../Icons';
import Loader from '../Loader';
import { isEmpty } from 'lodash';
import DeleteSharpIcon from '@material-ui/icons/DeleteSharp';
import AddIcon from '@material-ui/icons/Add';
import CreateIcon from '@material-ui/icons/Create';
import FolderIcon from '@material-ui/icons/Folder';
import DescriptionIcon from '@material-ui/icons/Description';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import BackupIcon from '@material-ui/icons/Backup';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ViewListIcon from '@material-ui/icons/ViewList';

import { AddFolderModal, DeleteFolderModal, EditFolderModal, AddFileModal, DeleteFileModal } from '../Modals';

import './index.css';

import { xhrClient } from '../../xhrClient';

class DocTreeView extends React.Component {
  
  constructor(props) {
    super(props);   

    this.state = {
      docs: [],
      oepn: false,
      user: JSON.parse(localStorage.getItem('user') || '{}'),
      modal: null,
      node: null,
      
      openAddFolderModal: false,
      openEditFolderModal: false,
      openDeleteFolderModal: false,

      openAddFileModal: false,
      openEditFileModal: false,
      openDeleteFileModal: false,
      openViewHistoryFileModal: false,

      openAddRootFolderModal: false,
      roles: []
    }; 
  }

  componentWillMount() {
    this.setState({open: true});

    const { user } = this.state;
    const { Role } = user;

    xhrClient.get(Role === 'ADMIN' ? 'documents' : 'documents/my')
      .then((res) => {
        const data = res.data || {};
        let docs = [];

        if(data.success === true) {
          docs = data.data || [];
        }  

        this.setState({open: false, docs});      
      })
      .catch(error => {
        this.setState({open: false});
      })  

    xhrClient.get('roles')
      .then((res) => {
        const data = res.data || {};

        if(data.success === true) {
          this.setState({roles: data.data || []});
        }  

        this.setState({open: false});        
      })
      .catch(error => {
        this.setState({open: false});  
      })   
  }

  onLabelClick = $event => {
    $event.preventDefault();
  }

  handleOpen = ($event, node, modal) => {
    $event.preventDefault();

    this.setState({[modal]: true, modal, node});
  }

  handleClose = () => {
    const { modal } = this.state;
    this.setState({[modal]: false, modal: null, node: null});
  }

  render() {
    const classes = makeStyles({
      root: {
        height: 110,
        flexGrow: 1,
        maxWidth: 400,
      },
    });

    const { docs, open, user, roles } = this.state;
    const { node } = this.state;
    const { Role } = user;

    const nodeLabel = (node) => (
      <div className="row" style={{marginBottom: 0}}>
        <div className="col s9 text-left">{node.Type === "Folder" && <FolderIcon />} {node.Type === "File" && <DescriptionIcon />} {node.Name}</div>
        {Role === "ADMIN" && <div className="col s3 text-right">
          <Tooltip title="Delete Folder"><DeleteSharpIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openDeleteFolderModal')} /></Tooltip>&nbsp;
          <Tooltip title="Add Sub Folder"><AddIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openAddFolderModal')} /></Tooltip>&nbsp;
          <Tooltip title="Edit Folder"><CreateIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openEditFolderModal')} /></Tooltip>
        </div>}
        {Role !== "ADMIN" && node.Type === "File" && <div className="col s3 text-right">
          <Tooltip title="Delete File"><DeleteSharpIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openDeleteFileModal')} /></Tooltip>&nbsp;
          <Tooltip title="Edit File"><AddIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openEditFileModal')} /></Tooltip>&nbsp;
          <Tooltip title="View File History"><VisibilityIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openViewHistoryFileModal')} /></Tooltip>&nbsp;
        </div>}
        {Role !== "ADMIN" && node.Type === "Folder" && <div className="col s3 text-right">
          <Tooltip title="Add File"><BackupIcon fontSize="small" onClick={($event) => this.handleOpen($event, node, 'openAddFileModal')} /></Tooltip>&nbsp;
        </div>}
      </div>
    )

    const renderTree = (nodes) => (
      <TreeItem key={nodes.DocId} nodeId={nodes.DocId} label={nodeLabel(nodes)} collapseIcon={<MinusSquare />} expandIcon={<PlusSquare />} onLabelClick={($event) => this.onLabelClick($event, nodes)}>
        {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
      </TreeItem>
    );

    return (
      <div className="row">
        <Loader open={open} />
        {this.state.openAddFolderModal && <AddFolderModal open={this.state.openAddFolderModal} node={node} handleClose={this.handleClose.bind(this)} roles={roles} />}
        {this.state.openDeleteFolderModal && <DeleteFolderModal open={this.state.openDeleteFolderModal} node={node} handleClose={this.handleClose.bind(this)} />}
        {this.state.openEditFolderModal && <EditFolderModal open={this.state.openEditFolderModal} node={node} handleClose={this.handleClose.bind(this)} roles={roles} />}

        {this.state.openAddFileModal && <AddFileModal open={this.state.openAddFileModal} node={node} handleClose={this.handleClose.bind(this)} />}
        {this.state.openDeleteFileModal && <DeleteFileModal open={this.state.openDeleteFileModal} node={node} handleClose={this.handleClose.bind(this)} />}

        <div className="col s3"></div>
        {!isEmpty(docs) && <div className="col s6 text-left">
          {docs.map(doc => {
            return <TreeView
              className={classes.root}
              key={doc.DocId}
            >
              {renderTree(doc)}
            </TreeView>
          })}
        </div>}
        {isEmpty(docs) && !open && <div className="col s6 text-right">
          <Button variant="contained" color="secondary" onClick={($event) => this.handleOpen($event, null, 'openAddRootFolderModal')}>
            Add Root Folder <AddIcon fontSize="small" />
          </Button>
        </div>}
        <div className="col s3"></div>
      </div>
    );
  }
}

export default DocTreeView;
