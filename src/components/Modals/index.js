
import React from 'react';

import Loader from '../Loader';
import { isEmpty } from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import { xhrClient } from '../../xhrClient';

import './index.css';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export const AddFolderModal = (props) => {

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const [open, setOpen] = React.useState(false);
  const [role, setRole] = React.useState([]);
  const [name, setName] = React.useState('');   
  const { node } = props;
  let roles = props.roles || [];
  if(node.ParentId && node.Roles.indexOf('ALL') < 0) {
    roles = roles.filter(r => node.Roles.indexOf(r.ID) > -1)
  } 

  const handleClose = () => {
    props.handleClose();
  };

  const handleRoleChange = (event) => {
    setRole(event.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleRequest = () => {
    setOpen(true); 

    if(isEmpty(name) || isEmpty(role)) {
      alert('Folder name and roles are mandatory fields');
      return;
    }

    xhrClient.post('documents', {name, roles: role, parentId: node.DocId, parentPath: node.Path, parentIdPath: node.ParentIdPath})
      .then((res) => {
        const data = res.data || {};

        if(data.success === true) {
          alert('Folder added successfully');
          window.location.reload();
        } else {
          alert('Something went wrong, pls try again after some time.');
        } 

        setOpen(false);        
      })
      .catch(error => {
        alert('Something went wrong, pls try again after some time.');
        setOpen(false);   
      })  
  }

  return (
    <div>
      <Loader open={open} />

      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.open} disableBackdropClick={true}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Add Folder
        </DialogTitle>
        <DialogContent dividers>          
          <form noValidate autoComplete="off" style={{width: '400px'}}>
            <div className="row">
              <TextField label="Folder Name" variant="outlined" className="col s12" onChange={handleNameChange} />
            </div>
            <div className="row">
              <FormControl variant="outlined" className="col s12">
                <InputLabel>Roles</InputLabel>
                <Select
                  multiple
                  value={role}
                  onChange={handleRoleChange}
                  input={<Input />}
                  renderValue={(selected) => selected.join(', ')}
                  MenuProps={MenuProps}
                >
                  {roles.map((r) => (
                    <MenuItem key={r.ID} value={r.ID}>
                      <Checkbox checked={role.indexOf(r.ID) > -1} />
                      <ListItemText primary={r.Name} />
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleRequest} color="primary">
            Save
          </Button>

          <Button variant="contained" onClick={handleClose} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}


export const EditFolderModal = (props) => {

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  
  const { node } = props;
  const roles = props.roles || [];

  const [open, setOpen] = React.useState(false);
  const [role, setRole] = React.useState((node && node.Roles) || []);

  const handleClose = () => {
    props.handleClose();
  };

  const handleRoleChange = (event) => {
    setRole(event.target.value);
  };

  const handleRequest = () => {
    setOpen(true); 

    if(isEmpty(role)) {
      alert('Folder roles are mandatory fields');
      return;
    }

    xhrClient.post(`documents/${node.DocId}`, {roles: role})
      .then((res) => {
        const data = res.data || {};

        if(data.success === true) {
          alert('Folder updated successfully');
          window.location.reload();
        } else {
          alert('Something went wrong, pls try again after some time.');
        } 

        setOpen(false);        
      })
      .catch(error => {
        alert('Something went wrong, pls try again after some time.');
        setOpen(false);   
      })  
  }

  return (
    <div>
      <Loader open={open} />

      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.open} disableBackdropClick={true}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Edit Folder
        </DialogTitle>
        <DialogContent dividers>          
          <form noValidate autoComplete="off" style={{width: '400px'}}>
            <div className="row">
              <b>Name:</b> {node && node.Name}
            </div>
            <div className="row">
              <FormControl variant="outlined" className="col s12">
                <InputLabel>Roles</InputLabel>
                <Select
                  multiple
                  value={role}
                  onChange={handleRoleChange}
                  input={<Input />}
                  renderValue={(selected) => selected.join(', ')}
                  MenuProps={MenuProps}
                >
                  {roles.map((r) => (
                    <MenuItem key={r.ID} value={r.ID}>
                      <Checkbox checked={role.indexOf(r.ID) > -1} />
                      <ListItemText primary={r.Name} />
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleRequest} color="primary">
            Save
          </Button>

          <Button variant="contained" onClick={handleClose} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}


export const DeleteFolderModal = (props) => {

  const [open, setOpen] = React.useState(false);  
  const { node } = props;

  const handleClose = () => {
    props.handleClose();
  };

  const handleRequest = () => {
    setOpen(true); 

    xhrClient.delete(`documents/${node.DocId}`)
      .then((res) => {
        const data = res.data || {};

        if(data.success === true) {
          alert('Folder deleted successfully');
          window.location.reload();
        } else {
          alert('Something went wrong, pls try again after some time.');
        } 

        setOpen(false);        
      })
      .catch(error => {
        alert('Something went wrong, pls try again after some time.');
        setOpen(false);   
      })  
  }

  return (
    <div>
      <Loader open={open} />

      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.open} disableBackdropClick={true}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Delete Folder
        </DialogTitle>
        <DialogContent dividers>          
          <div className="row">
            <Typography>
              Are you sure you want to delete folder <b>{node && node.Name}</b>? If yes, it will remove all it's sub folders as well.
            </Typography>
          </div>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleRequest} color="primary">
            Yes
          </Button>

          <Button variant="contained" onClick={handleClose} color="secondary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export const AddFileModal = (props) => {

  const [open, setOpen] = React.useState(false);
  const [file, setFile] = React.useState({});
  const [name, setName] = React.useState('');   
  const { node } = props;

  const handleClose = () => {
    props.handleClose();
  };

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleRequest = () => {
    setOpen(true); 

    if(isEmpty(name) || isEmpty(file.name)) {
      alert('File name and file are mandatory fields');
      return;
    }

    const formData = new FormData();

    formData.append('name', name);
    formData.append('parentId', node.DocId);
    formData.append('parentPath', node.Path);
    formData.append('parentIdPath', node.ParentIdPath);
    formData.append('roles', node.Roles);
    formData.append('doc', file, file.name)

    xhrClient.post('documents/my', formData)
      .then((res) => {
        const data = res.data || {};

        if(data.success === true) {
          alert('File added successfully');
          window.location.reload();
        } else {
          alert('Something went wrong, pls try again after some time.');
        } 

        setOpen(false);        
      })
      .catch(error => {
        alert('Something went wrong, pls try again after some time.');
        setOpen(false);   
      })  
  }

  return (
    <div>
      <Loader open={open} />

      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.open} disableBackdropClick={true}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Add File
        </DialogTitle>
        <DialogContent dividers>          
          <form noValidate autoComplete="off" style={{width: '400px'}}>
            <div className="row">
              <TextField label="File Name" variant="outlined" className="col s12" onChange={handleNameChange} />
            </div>
            <div className="row">
              <FormControl variant="outlined" className="col s12">
                <input type="file" accept=".pdf,.txt" className="col s12" onChange={handleFileChange} />
              </FormControl>
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleRequest} color="primary">
            Save
          </Button>

          <Button variant="contained" onClick={handleClose} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export const DeleteFileModal = (props) => {

  const [open, setOpen] = React.useState(false);  
  const { node } = props;

  const handleClose = () => {
    props.handleClose();
  };

  const handleRequest = () => {
    setOpen(true); 

    xhrClient.delete(`documents/my/${node.DocId}`)
      .then((res) => {
        const data = res.data || {};

        if(data.success === true) {
          alert('File deleted successfully');
          window.location.reload();
        } else {
          alert('Something went wrong, pls try again after some time.');
        } 

        setOpen(false);        
      })
      .catch(error => {
        alert('Something went wrong, pls try again after some time.');
        setOpen(false);   
      })  
  }

  return (
    <div>
      <Loader open={open} />

      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.open} disableBackdropClick={true}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Delete File
        </DialogTitle>
        <DialogContent dividers>          
          <div className="row">
            <Typography>
              Are you sure you want to delete file <b>{node && node.Name}</b>? If yes, it will remove all it's old versions as well.
            </Typography>
          </div>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleRequest} color="primary">
            Yes
          </Button>

          <Button variant="contained" onClick={handleClose} color="secondary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}