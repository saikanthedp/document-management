const express = require('express'),
	bodyParser = require('body-parser'),
	cors = require('cors'),
	fileUploader = require("express-fileupload"),
	logger = require('../src/utils/logger.util'),
	_ = require('lodash');

class ExpressSetup {

	/**
		Creates the express server
	**/
	init() {
		const app = express();

		app.use(cors({credentials: true, origin: true}));		
		app.use(bodyParser.json({limit: '25mb'}));
		app.use(bodyParser.urlencoded({limit: '25mb', extended: true}));		
		
		app.use((req, res, next) => {
      		logger.log(
      			`Request received ${_.get(req, 'method', 'N/A')} > ${_.get(req, 'originalUrl', 'N/A')}: ${JSON.stringify(_.get(req, 'body', {}))}`, 
      			"info"
      		);
			
			next();
	   });

		app.use(fileUploader());

	    return app;
	}
}

module.exports = new ExpressSetup();