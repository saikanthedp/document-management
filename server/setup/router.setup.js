const _ = require('lodash'),
	fs = require('fs'),
	logger = require('../src/utils/logger.util'),
	environment = require('../src/utils/environment.util');

class RoutersSetup {

	/**
		When app starts load routes
	**/
	init(app) {		
		const routes_path = `${environment.getConfigValue('root_dir')}/src/routers`;
		
		try {
			logger.log(`Loading routers`, 'info');

			fs.readdirSync(routes_path).forEach(file => {				
				if(file.split(".").length !== 3) {
					return;
				}

				const file_name_splits = file.split("."),
					ext = file_name_splits.pop(),
					router_ext = file_name_splits.pop(),
					router_name = file_name_splits.pop();

				if(ext === 'js' && router_ext === 'router') {
					try {
						const router = require(`${routes_path}/${file}`);
						app.use(`/${router_name.toLowerCase()}`, router.getRouters());
						logger.log(`${router_name} router loaded`, 'info');
					}
					catch(error) {
						logger.log(error, 'error');
						console.log(error);
						process.exit();
					}
				}
			});

			app.use((req, res, next) => {
				res.status(404).send('Requested entity was not found...');
			})

			logger.log(`Loaded all routers`, 'info');
		}
		catch(error) {
			logger.log(error, 'error');
			console.log(error);
			process.exit();
		}
	}
}

module.exports = new RoutersSetup();

