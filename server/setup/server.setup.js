const environment = require('../src/utils/environment.util');

class ServerSetup {

	/**
		Starts the server

		@param app object
	**/
	init(app) {
		const port = environment.getConfigValue('port', '9100');

		app.listen(port, function () {
	        console.log(`server is started and running on port: ${port}`);
	        console.log('');
	    })
	}
}

module.exports = new ServerSetup();