const _ = require('lodash'),
    service = require('../services/api.service'),
    { SUCCESS, FAILURE } = require('../utils/constants.util'),
    { i18n } = require('../utils/i18n.util'),
    response = require('../utils/response.util'),
    logger  = require('../utils/logger.util');

class APIController {

    async getRoles(req, res){
        try {
            const data = await service.getRoles();

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('roles_get_200'), 'roles_get_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('roles_get_400'), 'roles_get_400');
        }
    }

    async doLogin(req, res){
        try {
            const data = await service.doLogin(req.body.uname, req.body.pwd);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('user_login_200'), 'user_login_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('user_login_400'), 'user_login_400');
        }
    }
}

module.exports = new APIController();