const _ = require('lodash'),
    service = require('../services/document.service'),
    { SUCCESS, FAILURE } = require('../utils/constants.util'),
    { i18n } = require('../utils/i18n.util'),
    response = require('../utils/response.util'),
    logger  = require('../utils/logger.util');

class DocumentController {

    // Admin functions
    
    async getAllDocuments(req, res){
        try {
            const data = await service.getAllDocuments(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_get_admin_200'), 'docs_get_admin_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_get_admin_400'), 'docs_get_admin_400');
        }
    }

    async addDocument(req, res){
        try {
            const data = await service.addDocument(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_add_admin_200'), 'docs_add_admin_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_add_admin_400'), 'docs_add_admin_400');
        }
    }

    async updateDocument(req, res){
        try {
            const data = await service.updateDocument(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_update_admin_200'), 'docs_update_admin_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_update_admin_400'), 'docs_update_admin_400');
        }
    }

    async deleteDocument(req, res){
        try {
            const data = await service.deleteDocument(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_del_admin_200'), 'docs_del_admin_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_del_admin_400'), 'docs_del_admin_400');
        }
    }


    // User functions

    async getMyDocuments(req, res){
        try {
            const data = await service.getMyDocuments(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_get_my_200'), 'docs_get_my_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_get_my_400'), 'docs_get_my_400');
        }
    }

    async addMyDocument(req, res){
        try {
            const data = await service.addMyDocument(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_add_my_200'), 'docs_add_my_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_add_my_400'), 'docs_add_my_400');
        }
    }

    async updateMyDocument(req, res){
        try {
            const data = await service.updateMyDocument(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_update_my_200'), 'docs_update_my_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_update_my_400'), 'docs_update_my_400');
        }
    }

    async deleteMyDocument(req, res){
        try {
            const data = await service.deleteMyDocument(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_del_my_200'), 'docs_del_my_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_del_my_400'), 'docs_del_my_400');
        }
    }

    async getMyDocumentById(req, res){
        try {
            const data = await service.getMyDocumentById(req);

            if(data instanceof Error) {
                return response.send(res, FAILURE, (data.code), data.code, null);
            }

            response.send(res, SUCCESS, ('docs_get_my_doc_200'), 'docs_get_my_doc_200', data);
        } 

        catch(error) {  
            logger.log(error.stack, 'error');
            response.send(res, FAILURE, ('docs_get_my_doc_400'), 'docs_get_my_doc_400');
        }
    }
}

module.exports = new DocumentController();