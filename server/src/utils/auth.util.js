const _ = require('lodash'),
	{
		FAILURE,
		META_SETUP_DB
	} = require('./constants.util'),	
	response = require('./response.util'),
	environment = require('./environment.util'),
	logger = require('./logger.util'),
	{ i18n } = require('./i18n.util'),
	{ readData } = require('../utils/data.util'),
	jwt = require('jsonwebtoken');

class AuthUtil {

	async isPublic(req, res, next) {
		const authToken = (req.headers["x-auth-token"] || '').trim();

		if(_.isEmpty(authToken)) {
			logger.log(('user-auth-token-missing-msg'), 'error');

			return response.send(res, FAILURE, ('user-auth-token-missing-msg'), ('user-auth-token-missing-code'), {}, 401);
		}

		const User = await this._decodeAuthToken(authToken);
		let Roles = await readData('roles');
		Roles = Roles.filter(r => r.RoleId !== 'ADMIN').map(r => r.RoleId);

		if(_.isNull(User)) {
			logger.log(('user-auth-failed-msg'), 'error');

			return response.send(res, FAILURE, ('user-auth-failed-msg'), ('user-auth-failed-code'), {}, 401);
		} else if(Roles.indexOf(User.UserRole) < 0) {
			logger.log(('user-not-public-msg'), 'error');

			return response.send(res, FAILURE, ('user-not-public-msg'), ('user-not-public-code'), {}, 401);
		}
		_.set(req, 'User', User);

		next();
	}

	async isAdmin(req, res, next) {
		const authToken = (req.headers["x-auth-token"] || '').trim();

		if(_.isEmpty(authToken)) {
			logger.log(('user-auth-token-missing-msg'), 'error');

			return response.send(res, FAILURE, ('user-auth-token-missing-msg'), ('user-auth-token-missing-code'), {}, 401);
		}

		const User = await this._decodeAuthToken(authToken);
		if(_.isNull(User)) {
			logger.log(('user-auth-failed-msg'), 'error');

			return response.send(res, FAILURE, ('user-auth-failed-msg'), ('user-auth-failed-code'), {}, 401);
		} else if(User.UserRole !== 'ADMIN') {
			logger.log(('user-not-admin-msg'), 'error');

			return response.send(res, FAILURE, ('user-not-admin-msg'), ('user-not-admin-code'), {}, 401);
		}
		_.set(req, 'User', User);

		next();
	}

	async _decodeAuthToken(token) {
		const tokenInfo = await jwt.verify(token, environment.getConfigValue('jwt_secret_key'));

		if(tokenInfo) {
			const userId = _.get(tokenInfo, 'ID', null);
			const service = require('../services/api.service');

			if(userId) {
				const user = await service.getUserById(userId);

				return user;
			}

			logger.log(('user-auth-user-not-found-msg'), 'error');
		}
		else {
			logger.log(('jwt-secret-key-not-found-msg'), 'error');
		}

		return null;
	}

	_getToken(user) {
	    const signObject = {
	        ID: user.ID,
	        Role: user.Role
	    };
	    const jwt_secret_key = environment.getConfigValue('jwt_secret_key');

	    if(_.isEmpty(jwt_secret_key)) {
	    	logger.log(('jwt-secret-key-not-found-msg'), 'error');

	    	return null;
	    }

	    return jwt.sign(signObject, jwt_secret_key, {expiresIn: "7d"});
	}
}

const authUtil = new AuthUtil();

module.exports = {
	isPublic: authUtil.isPublic.bind(authUtil),
	isAdmin: authUtil.isAdmin.bind(authUtil),
	getToken: authUtil._getToken.bind(authUtil)
}

