const logger = require('./logger.util'),
	environment = require('./environment.util'),
	fs = require('fs')
	root_dir = environment.getConfigValue('root_dir');


/*
	user
	{
		UserId: UUID/String,
		UserName: String,
		UserEmail: String,
		UserMobile: String,
		UserPwd: String,
		UserRole: UUID/String,
		LostLoggedIn: Date/TimeStamp,
		LoggedIn: Boolean,
		CreatedAt: Date/TimeStamp,
		UpdatedAt: Date/TimeStamp,		
		Active: Boolean
	}
*/

/*
	role
	{
		RoleId: UUID/String,
		RoleName: String,		
		CreatedAt: Date/TimeStamp,
		UpdatedAt: Date/TimeStamp,
		Active: Boolean
	}
*/

/*
	document
	{
		DocId: UUID/String,
		Name: String,
		Path: String,
		Type: Enum(Folder, File),		
		ParentId: UUID/String,
		Roles: [UUID/String],
		CreatedAt: Date/TimeStamp,
		UpdatedAt: Date/TimeStamp,
		CreatedBy: UUID/String,
		Published: Boolean		
	}
*/

class DataUtil {

	async readData(collection) {

		return new Promise((resolve, reject) => {
			fs.readFile(`${root_dir}/data/${collection}.json`, (err, content) => {
				if(err) reject(err);
				const data = JSON.parse(content);
				
				resolve(data);
			})
		});
	}

	async writeData(collection, doc, rw = false) {

		return new Promise((resolve, reject) => {
			fs.readFile(`${root_dir}/data/${collection}.json`, (err, content) => {
				if(err) reject(err);

				let data;
				if(rw) {
					data = doc;
				} else {
					data = JSON.parse(content);
					data.push(doc);
				}
				
				fs.writeFile(`${root_dir}/data/${collection}.json`, JSON.stringify(data, null, 4), {encoding: 'utf8', flag: 'w'}, (err) => {
					if(err) reject(err);
					
					resolve(true);
				})				
			})
		});
	}
}


const dataUtil = new DataUtil();

module.exports = {
	readData: dataUtil.readData.bind(dataUtil),
	writeData: dataUtil.writeData.bind(dataUtil)
}