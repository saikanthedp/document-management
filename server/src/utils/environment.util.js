const _ = require('lodash'),
	path = require('path'),
	fs = require('fs');


class EnvironmentUtil {

	/**
		Sets the environment variables
		
		@param cb function(a call back)

		@return void
	**/
	init(cb) {
		// const config_file_path = this._getCLIConfigFile();
		// if(!fs.existsSync(config_file_path)) {
		// 	console.log(new Error(`Config file not found`));
		// 	return cb ? cb(false) : false;
		// }

		const config_file_path = '../../config.js';

		const env_configs = require(config_file_path); // JSON.parse(fs.readFileSync(config_file_path));
		if(_.isEmpty(env_configs)) {		
			console.log(new Error(`Configs not found`));
			return cb ? cb(false) : false;
		}

		process.env['config_env'] = JSON.stringify(env_configs);

		return cb ? cb(true) : env_configs;
	}

	/**
		Gets the environment variables

		@return object
	**/
	getEnvironmentConfigs() {
		return JSON.parse(_.get(process.env, 'config_env', '{}'));
	}

	/**
		Gets the environment value by path
		
		@param path string / array path
		@param defaultValue string

		@return string(on success) / ''(on not found)
	**/
	getConfigValue(path, defaultValue = '') {		
		const config_env = this.init();

		return _.get(config_env, path, defaultValue);
	}


	/**
		Get the environment

		@return string
	**/
	_getCLIEnv() {
		const cli_args = this._getCLIArgs();

		return _.get(cli_args, 'app_env');
	}

	/**
		Get the configfile

		@return string
	**/
	_getCLIConfigFile() {
		const cli_args = this._getCLIArgs();

		return _.get(cli_args, 'config');
	}

	/**
		Converts the command line argument to an object
		and returns the object

		@return object
	**/
	_getCLIArgs() {
		const cli_args = process.argv.slice(2),
			args = {}

		_.each(cli_args, (arg) => {
			const arg_splits = _.compact(arg.split('='));

			if(arg_splits.length === 2 && arg_splits[0] && arg_splits[1]) {
				args[arg_splits[0].toLowerCase()] = arg_splits[1].trim();
			}
		});

		return args; 
	}
}

module.exports = new EnvironmentUtil();