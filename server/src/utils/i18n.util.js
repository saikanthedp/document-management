const _ = require('lodash'),
	dict = {
	};

module.exports = {
	i18n: (key, ...params) => {
		let value = dict[key];
		_.each(params, (p, i) => {
			value = _.replace(value, new RegExp(`\\{${i}\\}`), p);
		});
		return value;
	},
};
