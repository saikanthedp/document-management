const _ = require('lodash'),
	logger = require('../utils/logger.util'),
	{
		FAILURE,
		SUCCESS
	} = require('../utils/constants.util'),
	{ i18n } = require('../utils/i18n.util'),
	{ v4: uuidv4 } = require('uuid'),
	{ setError } = require('../utils/exception.util'),
	{ readData, writeData } = require('../utils/data.util'),
	{ getToken } = require('../utils/auth.util');


class APIService {

	/**
		Fetch roles
	**/
	async getRoles() {

		try {
			const roles = await readData('roles');
			const data = roles.filter(r => r.Active === true && r.RoleId !== 'ADMIN').map(r => ({ID: r.RoleId, Name: r.RoleName}));	

			return data;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'roles_get_400');
			
			return error;
		}
	}

	/**
	 * do login
	 * @param username string
	 * @param password string
	 */
	async doLogin(username, password) {		
		
		try {
			const users = await readData('users');
			let user = users.find(r => (r.UserEmail === username || r.UserMobile === username));

			if(!user) {
				return setError('user_login_404');
			} else if(user.UserPwd !== password) {
				return setError('user_login_uname_pwd_400');
			}

			user = {
				"ID": user.UserId,
		        "Name": user.UserName,
		        "Email": user.UserEmail,
		        "Mobile": user.UserMobile,
		        "Role": user.UserRole,
			};

			const token = getToken(user);

			return {user, token};
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'user_login_400');

			return error;
		}
	}

	/**
	 * get user by id
	 * @param ID string
	 */
	async getUserById(ID) {
		try {
			const users = await readData('users');
			const user = users.find(r => r.UserId === ID);	

			delete user.UserPwd;

			return user;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'roles_get_400');
			
			return error;
		}
	}
}

module.exports = new APIService();
