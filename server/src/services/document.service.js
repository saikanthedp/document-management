const _ = require('lodash'),
	logger = require('../utils/logger.util'),
	{
		FAILURE,
		SUCCESS
	} = require('../utils/constants.util'),
	{ i18n } = require('../utils/i18n.util'),
	{ v4: uuidv4 } = require('uuid'),
	{ setError } = require('../utils/exception.util'),
	fs = require('fs'),
	{ readData, writeData } = require('../utils/data.util'),
	environment = require('../utils/environment.util'),
	rimraf = require("rimraf");

class DocumentService {

	// Admin services

	async getAllDocuments(req) {

		try {
			const documents = await readData('documents');			

			// Create root for top-level node(s)
			const documentsTree = [];
			// Cache found parent index
			const documentsTreeMap = {};

			documents.forEach(doc => {
				if(doc.Type === "File") return;

				// No ParentId means top level
				if (!doc.ParentId) return documentsTree.push(doc);

				// Insert doc as child of parent in documents array
				let parentIndex = documentsTreeMap[doc.ParentId];
				if (typeof parentIndex !== "number") {
					parentIndex = documents.findIndex(el => el.DocId === doc.ParentId);
					documentsTreeMap[doc.ParentId] = parentIndex;
				}
				if (!documents[parentIndex].children) {
					return documents[parentIndex].children = [doc];
				}

				/*const parentIndex = documents.findIndex(el => el.DocId === doc.ParentId);
				if (!documents[parentIndex].children) {
					return documents[parentIndex].children = [doc];
				}*/

				documents[parentIndex].children.push(doc);
			});

			return documentsTree;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_get_admin_400');
			
			return error;
		}
	}

	async addDocument(req) {

		try {
			const { body, User } = req;
			const { roles, name, parentId, parentPath, parentIdPath } = body;
			const { UserId } = User;

			let formatedname = name.replace(/\s{2,}/g, ' ');
			formatedname = name.replace(/[^a-z0-9+]/gi, ' ');
			const foldername = formatedname.replace(/\s/g, '').toLowerCase();
			const path = `${parentPath}/${foldername}`;
			const root = environment.getConfigValue('root_dir');

			if(fs.existsSync(`${root}${path}`)) {
				return setError('docs_add_exists_400');
			}

			const folderTree = _.compact(path.split('/'));
			let restPath = '';
			for(let i=0; i<folderTree.length; i++) {
				restPath = `${restPath}/${folderTree[i]}`;

				if(!fs.existsSync(`${root}${restPath}`)) {
					fs.mkdirSync(`${root}${restPath}`, {recursive: true, mode: 644});
				}
			}
			
			if(!fs.existsSync(`${root}${path}`)) {
				return setError('docs_add_disc_400');
			}

			const parentIdPathNew = _.compact(parentIdPath.split('/'));
			parentIdPathNew.push(parentId)

			const doc = {
				"DocId": uuidv4(),
				"Name": name,
				"Path": `${path}`,
				"ParentIdPath": `${parentIdPathNew.join('/')}`,
				"Type": "Folder",
				"ParentId": parentId,
				"Roles": roles,
				"CreatedAt": new Date().getTime(),
				"UpdatedAt": new Date().getTime(),
				"CreatedBy": UserId,
				"Published": true
			};

			const data = await writeData('documents', doc);

			return data;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_add_admin_400');
			
			return error;
		}
	}

	async updateDocument(req) {

		try {
			const { body, User, params } = req;
			const { roles } = body;
			const { UserId } = User;
			const { docid } = params;
			let found = false;

			const documents = await readData('documents');			
			const docs = documents.map(d => {
				if(d.DocId === docid) {
					_.set(d, 'Roles', roles);
					found = true;
				}

				if(d.ParentIdPath.split('/').indexOf(docid) > -1) {
					_.set(d, 'Roles', roles);
				}

				return d;
			});

			if(found) await writeData('documents', docs, true)

			return found;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_update_admin_400');
			
			return error;
		}
	}

	async deleteDocument(req) {

		try {
			const docId = req.params.docid;
			const documents = await readData('documents');			
			const doc = documents.find(d => d.DocId === docId);
			
			const removed = await this._removeFolders(doc.Path);

			if(removed) {
				const newdocuments = documents.filter(d => d.DocId !== docId && d.ParentIdPath.split('/').indexOf(docId) < 0);
				await writeData('documents', newdocuments, true);
			}

			return removed;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_del_admin_400');
			
			return error;
		}
	}

	async _removeFolders(docPath) {
		return new Promise((resolve) => {
			const root = environment.getConfigValue('root_dir');

			rimraf(`${root}${docPath}`, () => {	
				if(fs.existsSync(`${root}${docPath}`)) {
					resolve(false);
				} else {			
					resolve(true);
				}
			})
		})
	}

	// User services

	async getMyDocuments(req) {

		try {
			const documents = await readData('documents');
			const { User } = req;
			const { UserRole } = User;

			// Create root for top-level node(s)
			const documentsTree = [];
			// Cache found parent index
			const documentsTreeMap = {};

			documents.forEach(doc => {
				if(doc.Roles.indexOf(UserRole) < 0 && doc.Roles.indexOf('ALL') < 0) return;

				if(doc.Type === 'File' && doc.CreatedBy !== User.UserId) return;
				if(!doc.Published) return;

				delete doc.CreatedBy;

				// No ParentId means top level
				if (!doc.ParentId) return documentsTree.push(doc);

				// Insert doc as child of parent in documents array
				let parentIndex = documentsTreeMap[doc.ParentId];
				if (typeof parentIndex !== "number") {
					parentIndex = documents.findIndex(el => el.DocId === doc.ParentId);
					documentsTreeMap[doc.ParentId] = parentIndex;
				}
				if (!documents[parentIndex].children) {
					return documents[parentIndex].children = [doc];
				}

				/*const parentIndex = documents.findIndex(el => el.DocId === doc.ParentId);
				if (!documents[parentIndex].children) {
					return documents[parentIndex].children = [doc];
				}*/

				documents[parentIndex].children.push(doc);
			});

			return documentsTree;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_get_my_400');
			
			return error;
		}
	}

	async addMyDocument(req) {

		try {
			const { body, User, files } = req;
			const { name, parentId, parentPath, parentIdPath, roles } = body;
			const { UserId } = User;

			let formatedname = name.replace(/\s{2,}/g, ' ');
			formatedname = name.replace(/[^a-z0-9+]/gi, ' ');
			const filename = formatedname.replace(/\s/g, '').toLowerCase();
			const path = `${parentPath}`;
			const root = environment.getConfigValue('root_dir');

			const filetoBeUploaded = files.doc || {};
			if(_.isEmpty(filetoBeUploaded)) {
				return setError('docs_add_no_file_400');
			}

			if(fs.existsSync(`${root}${path}/${filename}.${this._getFileExt(filetoBeUploaded.name)}`)) {
				return setError('docs_add_exists_400');
			}

			const folderTree = _.compact(path.split('/'));
			let restPath = '';
			for(let i=0; i<folderTree.length; i++) {
				restPath = `${restPath}/${folderTree[i]}`;

				if(!fs.existsSync(`${root}${restPath}`)) {
					fs.mkdirSync(`${root}${restPath}`, {recursive: true, mode: 644});
				}
			}
			
			if(!fs.existsSync(`${root}${path}`)) {
				return setError('docs_add_disc_400');
			}
			
			const base64data = new Buffer(filetoBeUploaded.data, 'binary');
			const fileNametoBeUploaded = `${path}/${filename}.${this._getFileExt(filetoBeUploaded.name)}`;

			fs.writeFileSync(`${root}${fileNametoBeUploaded}`, base64data, {encoding: 'utf8', flag: 'w'});

			if(!fs.existsSync(`${root}${fileNametoBeUploaded}`)) {
				return setError('docs_add_disc_file_400');
			}

			const parentIdPathNew = _.compact(parentIdPath.split('/'));
			parentIdPathNew.push(parentId);

			const doc = {
				"DocId": uuidv4(),
				"Name": name,
				"Path": `${fileNametoBeUploaded}`,
				"ParentIdPath": `${parentIdPathNew.join('/')}`,
				"Type": "File",
				"ParentId": parentId,
				"Roles": (roles && roles.split(',')) || [],
				"CreatedAt": new Date().getTime(),
				"UpdatedAt": new Date().getTime(),
				"CreatedBy": UserId,
				"Published": true
			};

			const data = await writeData('documents', doc);

			return data;
		}
		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_add_my_400');
			
			return error;
		}
	}

	_getFileExt(filename) {
		return filename.split(".").pop();
	}

	async updateMyDocument(req) {

		try {
			const data = {};

			return data;
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_update_my_400');
			
			return error;
		}
	}

	async deleteMyDocument(req) {

		try {
			const docId = req.params.docid;
			const documents = await readData('documents');			
			const doc = documents.find(d => d.DocId === docId);
			const root = environment.getConfigValue('root_dir');

			if(fs.existsSync(`${root}${doc.Path}`)) {
				fs.unlinkSync(`${root}${doc.Path}`);

				if(fs.existsSync(`${root}${doc.Path}`)) 
					return setError('docs_del_my_not_400'); 
				else {
					const newdocuments = documents.filter(d => d.DocId !== docId);
					await writeData('documents', newdocuments, true);

					return true;
				}
			} else {
				return setError('docs_del_my_not_exist_400');
			}
		} 

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_del_my_400');
			
			return error;
		}
	}

	async getMyDocumentById(res) {
		try {
			return true;
		}

		catch(error) {
			logger.log(error.stack, 'error');
			_.set(error, 'code', 'docs_del_my_400');
			
			return error;
		}
	}
}

module.exports = new DocumentService();
