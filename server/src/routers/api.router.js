const _ = require('lodash'),
    path = require('path'),
    fs = require('fs'),
    express = require('express'),
    router = express.Router(),
    controller = require('../controllers/api.controller'),
    docController = require('../controllers/document.controller'),
    logger = require('../utils/logger.util'),
    { isPublic, isAdmin } = require('../utils/auth.util');

class APIRouter {

    getRouters(){
        try {
            // We can put the below routes into seperate route files later.

            /*
             user routers
            */
            // router.get('/user', isAdmin, controller.getUsers.bind(controller));
            // router.post('/user', isAdmin, controller.addUser.bind(controller));
            // router.post('/user/:id', isAdmin, controller.updateUser.bind(controller));            
            // router.get('/user/:id', isAdmin, controller.getUserById.bind(controller));
            // router.delete('/user/:id', isAdmin, controller.deleteUsersByID.bind(controller));
            router.post('/user/login', controller.doLogin.bind(controller));

            /*
             user roles routers
            */
            router.get('/roles', isAdmin, controller.getRoles.bind(controller));
            // router.post('/roles', isAdmin, controller.addRoles.bind(controller));
            // router.post('/roles/:id', isAdmin, controller.updateRole.bind(controller));            
            // router.get('/roles/:id', isAdmin, controller.getRoleById.bind(controller));
            // router.delete('/roles/:id', isAdmin, controller.deleteRoleByID.bind(controller));

            /*
             document management routes
            */
            router.get('/documents/my', isPublic, docController.getMyDocuments.bind(controller));
            router.get('/documents/my/:docid', isPublic, docController.getMyDocumentById.bind(controller));
            router.post('/documents/my', isPublic, docController.addMyDocument.bind(controller));
            router.post('/documents/my/:docid', isPublic, docController.updateMyDocument.bind(controller));
            router.delete('/documents/my/:docid', isPublic, docController.deleteMyDocument.bind(controller));

            router.get('/documents', isAdmin, docController.getAllDocuments.bind(controller));
            router.post('/documents', isAdmin, docController.addDocument.bind(controller));
            router.post('/documents/:docid', isAdmin, docController.updateDocument.bind(controller));
            router.delete('/documents/:docid', isAdmin, docController.deleteDocument.bind(controller));

            return router;
        }

        catch(error) {
            logger.log(error.stack, 'error');
        }
    }
}

module.exports = new APIRouter();