const environment = require('./server/src/utils/environment.util'),
	logger = require('./server/src/utils/logger.util'),
	router = require('./server/setup/router.setup'),
	express = require('./server/setup/express.setup'),
	server = require('./server/setup/server.setup'),
	_ = require('lodash'),
	fs = require('fs');

console.log('Setting environment variables...');

environment.init(has_set => {
	/*
		If enviroment values not found or config file is not found
		we don't initiate the app. Kills the process and show the 
		error on the console.
	*/
	if(has_set) {
		console.log('Setting environment variables is done.');
		console.log('');

		//init logger
		console.log('Initiating logger...');
		logger.init();
		console.log('Initiating logger is done');
		console.log('');

		//init express
		console.log('Initiating express...');
		const app = express.init();
		console.log('Initiating express is done');
		console.log('');

		//init router
		console.log('Loading routers...');
		const routers = router.init(app);
		console.log('Loading routers is done');
		console.log('');	

		//init express
		console.log('Initiating server...');
		server.init(app);
		console.log('Initiating server is done');
		console.log('');
	}
})
