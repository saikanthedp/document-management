# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Document Management Repository :

The system will have folders and sub-folders, which are managed by the admin only. User’s cannot add new folders/sub-folders. 

Users are added to user groups and each user group has access to certain folder/sub-folders in the system. When a user logs in they need to see the folder structure that they have access to from the parent down. 

E.g. 

1. Assume there are 2 folders A & B.

2. A has sub-folders A1, A2 & A3.

3. B has sub-folders B1 & B2. 

4. If a user belongs to a group which is assigned access to A1, when they log in they will see folder A & only folder A1 inside it. Inside A1 the user should be able to see all sub-folder.

5. If a user belongs to a group which is assigned folder A3 & B, when they log in they will folder A(with A3 inside it) and folder B with all folder inside B. 

6. If the admin adds another folder B3 inside B, the users having access to folder B should be able to see B3 as well. 
 

Each folder has documents, which can have multiple versions. For the sake of simplicity assume that the documents are just text files or PDFs. When a user views a document they should see the latest version of the document, and have an ability to view older versions if they want. A user can update the document if they have access to it(since we are only doing text or PDFs just assume that the user goes to the document and uploads a new version using an upload button), when they do it should create a new version of the document which then becomes the latest version.


### How do I get set up? ###

We just need NodeJS(>8+) to be installed in machine to get this app run.

We are using ReactJS on client side & NodeJS on server side.

Just navigate to the path where you have cloned the repo and do the following steps to start the app

step 1: npm install
step 2: npm start

app will be opened automatically in a web browser(default is chrome). If not openned, open the http://localhost:4200 in any web browser.


It has no database software. It completely works with json files. We have data files in /server/data folder.

We have three access roles and each has been assinged one user each. You can add new roles and users to it in files directly. There is no roles and user managemen yet.

Admin:
sunny@document.in
admin@123

After login admin you can add folders and sub folders and assign roles. 
Note: don't remove root folder. 

Users:
gururaj@document.in
public@123

saikanth@document.in
special@123


After login to user panel user can upload files(either txt or pdf). User can see his/her uploaded files only. They can't see other user's files.


Still there is pending work...

